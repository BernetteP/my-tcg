export default class Hand {
    constructor (config) {
        if(typeof config !== 'object' || config === null) {
            throw new Error("parameter should be an object")
        }
        if(!Array.isArray(config.cards)) {
            throw new Error("object.cards should contain an array") 
        }
        if(config.limit === undefined) {
            this.limit = 7;
        }
        else {
            this.limit = config.limit;
        }
        if(typeof this.limit !== 'number') {
            throw new Error("Limit should be a number");
        }
        if(this.limit < config.cards.length) {
            throw new Error("Cannot handed more than limit cards");
        }
        this.cards = config.cards;
    }
    addCard(card) {
        if(typeof card !== 'object' || card === null) {
            return false;
        }
        else {
            if(this.cards.length < this.limit) {
                this.cards.push(card);
                return true;
            }
            else {
                return false;
            }
        }
    }

    removeCard(pos) {
        if(typeof pos !== 'number' || pos > this.cards.length) {
            return false;
        }
        else {
            let removedCard = this.cards.splice(pos, 1);
            return removedCard[0];
        }
    }
    getAllCards() {
        if(this.cards.length > 0) {
            let allCards = this.cards;
            return allCards;
        }
        else {
            return false;
        }
    }

    getCardsCount() {
        if(this.cards.length > 0) {
            let numOfCards = this.cards.length;
            return numOfCards;
        }
        else {
            return false;
        }
    }
}