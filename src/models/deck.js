
export default class Deck {
    constructor (config) {
        if(typeof config !== 'object' || config === null) {
            throw new Error("parameter should be an object")
        }
        if(!Array.isArray(config.cards) && config.cards.length >= 0) {
            throw new Error("object.cards should contain an array"); 
        }
        this.cards = config.cards;
    }

    shuffle () {
        if(Array.isArray(this.cards) && this.cards.length > 2){
            this.cards.sort(function() {
                return 0.5 - Math.random()
            })
            return true;
        }
        else {
            return false;
        }
    }

    insertAt(card, pos) {
        if(typeof(card) !== 'object' || card === null) {
            throw new Error("Deck.insertAt expect its first parameter to be an object");
        }
        if(typeof(pos) !== "number") {
            throw new Error("Deck.insertAt expect its second parameter to be an integer");
        }
        if(this.cards.length >= pos){
            this.cards.splice(pos, 0, card);
        }
        else {
            throw new Error("Deck.insertAt expect its second parameter to be inferior or equal to cards.length");
        }
    }

    draw () {
        if(this.cards.length > 0) {
            let firstCard = this.cards.shift();
            return firstCard;
        }
        else {
            return false;
        }
    }

    getCardsCount () {
        if(this.cards.length > 0) {
            let totalOfCards = this.cards.length;
            return totalOfCards;
        }
        else {
            return false;
        }
    }
}
// let con = {"cards" : [{"face":"card-1"}, {"face":"card-2"}, {"face":"card-3"}]};
// let toto = new Deck(con);
// console.log("toto getCardsCount :", toto.getCardsCount());
// console.log("con : ", con);

