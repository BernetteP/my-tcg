import EventManager from '../eventManager';

export default class Pawn extends EventManager {
    constructor(life, strength, def) {
        if(typeof life !== 'number' || typeof strength !== 'number' || typeof def !== 'number') {
            throw new Error("All parameters should be numbers");
        }
        if(life <= 0 || strength <= 0 || def <= 0) {
            throw new Error("All parameters should be positives");
        }
        super();
        this.life = life;
        this.strength = strength;
        this.def = def;
    }

    getLife() {
        return this.life;
    }

    getStrength() {
        return this.strength;
    }

    getDef() {
        return this.def;
    }

    attack(target) {
        if(typeof target !== 'object' || target === null) {
            throw new Error("parameter should be an object")
        }
        target.receiveAttack(this);
    }

    receiveAttack(opponent, strikeBack = false) {
        if(typeof opponent !== 'object' || opponent === null) {
            throw new Error("first parameter expected to be an object");
        }
        if(strikeBack === true) {
            this.life = this.life - opponent.getDef();
        }
        else {
            this.life = this.life - opponent.getStrength();
            opponent.receiveAttack(this, true);
        }
    }
}


