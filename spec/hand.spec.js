import Hand from '../src/models/hand';

describe("Hand", function() {
    describe("constructor", function() {
        it("should confirm param is an object", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            expect(() => new Hand(config)).not.toThrow();
        })

        it("should confirm param.cards is an array", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            
            expect(() => new Hand(config)).not.toThrow();
        })

        it("Param is not an object", function() {
            let config = ['je ne suis pas un objet', 42, true, null, undefined];
            for (var i = 0; i < config.length; i+=1) {
                expect(() => {new Hand(config[i])}).toThrow(new Error("parameter should be an object"));
            }
        })

        it("Param.cards is not an array", function() {
            let config = {"cards" : "je ne suis pas un tableau"};
                expect(() => {new Hand(config)}).toThrow(new Error("object.cards should contain an array"));
        })

        it("no limit property specified, more than 7 cards", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}
            , {"face":"card-3"}, {"face":"card-4"}, {"face":"card-5"}
            , {"face":"card-6"}, {"face":"card-7"}, {"face":"card-8"}]};
            expect(() => new Hand(config)).toThrow(new Error("Cannot handed more than limit cards"));
        })

        it("limit property specified, more than limit cards", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}
            , {"face":"card-3"}, {"face":"card-4"}, {"face":"card-5"}],
                            "limit" : 4};
            expect(() => new Hand(config)).toThrow(new Error("Cannot handed more than limit cards"));
        })

        it("limit is specified and is not a number", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
                        "limit" : ['je ne suis pas un objet', {}, true, null, undefined]};
            for (var i = 0; i < config.length; i+=1) {
            expect(() => new Hand(config)).toThrow(new Error("Limit should be a number"));
            }
        })

        it("limit is specified and is a number", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
                        "limit" : 8};
            expect(() => new Hand(config)).not.toThrow();
        })

        it("Shouldn't throw error when instanciate with no limit specified", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            expect(() => new Hand(config)).not.toThrow();
        })

        it("shouldn't throw error when instanciate with limit specified", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
        "limit" : 4};
        expect(() => new Hand(config)).not.toThrow();
        })
    })

    describe("addCard", function() {
        it("should param to be an object", function() {
            let card = {"face":"card-3"};
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let hand = new Hand(config);
            let addCard = hand.addCard(card);
            expect(addCard).toBe(true);
        })

        it("param is not an object", function() {
            let card = ["je ne suis pas un objet", 42, true, undefined, null];
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let hand = new Hand(config);
            let addCard;
            for(var i = 0; i < card.length; i+=1) {
                addCard = hand.addCard(card[i]);
                expect(addCard).toBe(false);
            }
        })

        it("should be one card up in the hand", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let hand = new Hand(config);
            let numOfCards = config.cards.length;
            hand.addCard({"face":"card-3"});
            let newNumOfCards = config.cards.length;
            expect(newNumOfCards).toEqual(numOfCards + 1);
        })

        it("should one card added to hand, be the last one", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let hand = new Hand(config);
            let card = {"face":"card-3"};
            hand.addCard(card);
            let lastCard = config.cards.pop();
            expect(lastCard).toEqual(card);
        })

        it("same amounts of cards than limit", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
                            "limit" : 2};
            let hand = new Hand(config);
            let card = {"face":"card-3"};
            let addCard = hand.addCard(card);
            expect(addCard).toBe(false);
        })

        it("good amounts of cards than limit", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
                            "limit" : 3};
            let hand = new Hand(config);
            let card = {"face":"card-3"};
            let addCard = hand.addCard(card);
            expect(addCard).toBe(true);
        })
    })

    describe("removeCard", function() {
        it("param is not a number", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
                            "limit" : 3};
            let hand = new Hand(config);
            let pos = ['je ne suis pas un objet', true, null, undefined, {"face":"card-2"}];
            for(var i = 0; i < config.length; i+=1) {
                let removeCard = hand.removeCard(pos[i]);
                expect(removeCard).toBe(false);
            }
        })

        it("param is higher to number of cards", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
                            "limit" : 3};
            let hand = new Hand(config);
            let removeCard = hand.removeCard(3);
            expect(removeCard).toBe(false);
        })

        it("param is inferior to number of cards", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}],
                            "limit" : 3};
            let hand = new Hand(config);
            let card = {"face":"card-1"};
            let removeCard = hand.removeCard(0);
            expect(card).toEqual(removeCard);
        })

        it("should one card remove from hand", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let hand = new Hand(config);
            let numOfCards = config.cards.length;
            hand.removeCard(0);
            let newNumOfCards = config.cards.length;
            expect(newNumOfCards).toEqual(numOfCards - 1);
        })
    })

    describe("getAllCards", function() {
        it("should get all cards", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let hand = new Hand(config);
            let allCards = hand.getAllCards();
            expect(allCards.length).toEqual(2);
        })

        it("when no more cards in hand", function() {
            let config = {"cards" : []};
            let hand = new Hand(config);
            let allCards = hand.getAllCards();
            expect(allCards).toBe(false);
        })
    })

    describe("getCardCount", function() {
        it("should find the good number of cards", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let hand = new Hand(config);
            let numOfCards = hand.getCardsCount();
            expect(numOfCards).toEqual(2);
        })

        it("No more cards in deck", function() {
            let config = {"cards" : []};
            let hand = new Hand(config);
            let numOfCards = hand.getCardsCount();
            expect(numOfCards).toBe(false);
        })
    })
})