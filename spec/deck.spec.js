import Deck from '../src/models/deck';

describe("Deck", function() {
    
    describe("constructor", function() {
        it("should confirm param is an object", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            expect(() => new Deck(config)).not.toThrow();
        })

        it("should confirm param.cards is an array", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            
            expect(() => new Deck(config)).not.toThrow();
        })

        it("Param is not an object", function() {
            let config = ['je ne suis pas un objet', 42, true, null, undefined];
            for (var i = 0; i < config.length; i+=1) {
                expect(() => {new Deck(config[i])}).toThrow(new Error("parameter should be an object"));
            }
        })

        it("Param.cards is not an array", function() {
            let config = {"cards" : "je ne suis pas un tableau"};
                expect(() => {new Deck(config)}).toThrow(new Error("object.cards should contain an array"));
        })
    })

    describe("shuffle", function() {
        it("deck should be shuffled", function() {
            let config = {"cards" : [1, 2, 3, 4, 5]};
            let deck = new Deck(config);
            let shuffled = deck.shuffle();
            expect(config).not.toEqual(shuffled);
        })

        it("shuffle should return false", function() {
            let config = {"cards" : [1]};
            let deck = new Deck(config);
            let shuffled = deck.shuffle();
            expect(shuffled).toBe(false);

        })
    })

    describe("insertAt", function() {
        it("parameter one is not an object", function() {
            let card = ['je ne suis pas un objet', 42, true, null, undefined];
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);

            for (var i = 0; i < card.length; i+=1) {
                expect(() => {deck.insertAt(card[i], 1)}).toThrow(new Error("Deck.insertAt expect its first parameter to be an object"));
            }
        })

        it("parameter two is not a number", function() {
            let pos = ['je ne suis pas un objet', true, null, undefined, {"face":"card-2"}];
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);

            for (var i = 0; i < pos.length; i+=1) {
                expect(() => {deck.insertAt({"face":"card-3"}, pos[i])}).toThrow(new Error("Deck.insertAt expect its second parameter to be an integer"));
            }
        })

        it("parameter two is superior to cards number", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);
                expect(() => {deck.insertAt({"face":"card-3"}, config.cards.length + 1)}).toThrow(new Error("Deck.insertAt expect its second parameter to be inferior or equal to cards.length"));
        })

        it("should insert card to position", function() {
            var pos = [];
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);
            for(var i = 0; i <= config.cards.length; i+=1) {
                pos.push(i);
            }
            for(var j = 0; j < pos.length; j+=1) {
                expect(() => {deck.insertAt({"face":"card-3"}, pos[j])}).not.toThrow();
            }
        })

        it("should insert card to position with array comparison", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);
            let cards = config.cards;
            let cardsExpected = [{"face":"card-1"}, {"face":"card-3"}, {"face":"card-2"}];
            deck.insertAt({"face":"card-3"}, 1);
            expect(cards).toEqual(cardsExpected);
        })
    })

    describe("draw", function () {
        
        it("should one card taking of deck, be the first one", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);
            let expectedCard = {"face":"card-1"};
            let firstCard = deck.draw();
            expect(firstCard).toEqual(expectedCard);
        })
        
        it("should be one card down in the deck", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);
            let numOfCards = config.cards.length;
            deck.draw();
            let newNumOfCards = config.cards.length;
            expect(newNumOfCards).toEqual(numOfCards - 1);
        })

        it("should return false with an empty deck", function() {
            let config = {"cards" : []};
            let deck = new Deck(config);
            let draw = deck.draw();
            expect(draw).toBe(false);
        })
    })
    describe("getCardsCount", function() {
        it("should find the good number of cards", function() {
            let config = {"cards" : [{"face":"card-1"}, {"face":"card-2"}]};
            let deck = new Deck(config);
            let numOfCards = deck.getCardsCount();
            expect(numOfCards).toEqual(2);
        })

        it("No more cards in deck", function() {
            let config = {"cards" : []};
            let deck = new Deck(config);
            let numOfCards = deck.getCardsCount();
            expect(numOfCards).toBe(false);
        })
    })
})