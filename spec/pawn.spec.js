import Pawn from '../src/models/pawn';

describe("Pawn", function() {
    describe("constructor", function() {
        it("should confirm params are int", function() {
            let life = ['not number', {}, true, null, undefined];
            let strength = ['not number', {}, true, null, undefined];
            let def = ['not number', {}, true, null, undefined];
            for (var i= 0; i < life.length; i+=1) {
                expect(() => {new Pawn(life[i], strength[i], def[i])}).toThrow(new Error("All parameters should be numbers"));
            }
        })

        it("All parameters are positive", function() {
            let life = 12;
            let strength = 17;
            let def = 6;
            expect(() => {new Pawn(life, strength, def)}).not.toThrow();
        })

        it("If one of the parameters are negative", function() {
            let life = [-20, 20, 20, -20];
            let strength = [10, -10, 10, -10];
            let def = [30, 30, -30, -30];
            for (var i= 0; i < life.length; i+=1) {
                expect(() => {new Pawn(life[i], strength[i], def[i])}).toThrow(new Error("All parameters should be positives"));
            }
        })
    })
    describe("getLife", function() {
        it("getLife was caught", function() {
            let life = 26;
            let strength = 17;
            let def = 6;
            let pawn = new Pawn(life, strength, def);
            expect(pawn.getLife()).toEqual(26);
        })
    })

    describe("getStrength", function() {
        it("getStrength was caught", function() {
            let life = 26;
            let strength = 17;
            let def = 6;
            let pawn = new Pawn(life, strength, def);
            expect(pawn.getStrength()).toEqual(17);
        })
    })

    describe("getDef", function() {
        it("getDef was caught", function() {
            let life = 26;
            let strength = 17;
            let def = 6;
            let pawn = new Pawn(life, strength, def);
            expect(pawn.getDef()).toEqual(6);
        })
    })

    describe("attack/receiveAttack", function () {
        it("target should be an object", function() {
            let pawn = new Pawn(12, 12, 12);
            let target = ['je ne suis pas un objet', 42, true, null, undefined];
            for (var i = 0; i < target.length; i+=1) {
                expect(() => {pawn.attack(target[i])}).toThrow(new Error("parameter should be an object"));
            }
        })

        it("target is an object", function () {
            let pawn = new Pawn(100, 10, 20);
            let target = new Pawn (300, 20, 10);
            expect(() =>{pawn.attack(target)}).not.toThrow();
        })

        it("Should verify attack and receiveAttack", function() {
            let pawn = new Pawn(100, 10, 20);
            let target = new Pawn (300, 10, 20);
            pawn.attack(target);
            expect(pawn.getLife()).toBe(80)
            expect(target.getLife()).toBe(290)
        })

        it("opponent should be an object", function() {
            let pawn = new Pawn(100, 10, 20);
            let opponent = ['je ne suis pas un objet', 42, true, null, undefined];
            for (var i = 0; i < opponent.length; i+=1) {
                expect(() => {pawn.receiveAttack(opponent[i])}).toThrow(new Error("first parameter expected to be an object"));
            }
        })

        it("opponent is an object", function () {
            let pawn = new Pawn(100, 10, 20);
            let opponent = new Pawn (300, 20, 10);
            expect(() =>{pawn.receiveAttack(opponent)}).not.toThrow();
        }) 
    })
})