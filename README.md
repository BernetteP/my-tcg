# my-tcg

1. Lancer la commande "npm install".
2. Passer à node v11 : 
	"npm i -g nvm".
	"nvm install v11".
	"nvm use 11".
3. Lancer le projet "npm run gulp".
4. Ouvrir le "index.html" dans votre navigateur.
5. Lancer les tests "npm run test"


